# README #


### How do I get set up? ###

`git clone`
`composer install`
`php -S localhost:8080`


### How to use? ###

Enter the time of the start of the project and the estimated duration in the query string in the following format:

`localhost:8080/?t=2017-09-07-12-15&d=1`

### How to run the tests? ###

run `vendor/bin/phpunit`