<?php

namespace Utils;
class TimeHelper
{
    /**
     * @param \DateTime $time
     * @return \DateTime
     */
    public static function roundToHour(\DateTime $time)
    {
        if ($time->format('i')>30)
        {
            $time->setTime((int)$time->format('H')+1,0);
        }
        else
        {
            $time->setTime((int)$time->format('H'),0);
        }
        return $time;
    }

    /**
     * @param \DateTime $time
     * @return \DateTime static
     */
    public static function addOneHour(\DateTime $time)
    {
        return $time->add(\DateInterval::createFromDateString('+1 hours'));
    }

    public static function getString(\DateTime $time)
    {
        return $time->format('Y-m-d H:i:s');
    }
}