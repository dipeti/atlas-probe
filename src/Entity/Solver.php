<?php

namespace Entity;
use Utils\TimeHelper;

class Solver
{
    /**
     * @var Project
     */
    private $project;

    /**
     * @var \DateTime
     */
    private $today;

    /**
     * @var integer
     */
    private $remainingHours;

    /**
     * Solver constructor.
     * @param Project $project
     */
    public function __construct(Project $project)
    {
        $this->project = $project;
        $this->remainingHours = $project->getDuration()->h;
        $this->today = new \DateTime('now');
    }

    public function solve()
    {
        $startDate = $this->project->getStartDate();
        if (!$this->isDateWithinWorkingHours($this->project->getStartDate()))
        {
            echo 'We cannot start working on this project right away, because we are out of office... :( Please be patient.';
            $startDate = $this->getNextWorkingDate($this->project->getStartDate());
            echo '<br>We will start it at: ';
            echo TimeHelper::getString($startDate);

        }
        while ($this->remainingHours !== 0)
        {
            $startDate = TimeHelper::addOneHour($startDate);
            if ($this->isDateWithinWorkingHours($startDate))
            {
                $this->remainingHours -= 1;
            }
            else
            {
                $startDate = $this->getNextWorkingDate($startDate);
            }
        }

        return TimeHelper::getString($startDate);
    }

    private function isDateWithinWorkingHours(\DateTime $date)
    {
        $weekends = ['Saturday' =>'6', 'Sunday' => '7',];
        $dayNo = $date->format('N');
        $hour = $date->format('H');
        if($hour < 8 || 17 < $hour || in_array($dayNo,$weekends,true) )
        {
            return false;
        }
        return true;
    }

    /**
     * @param \DateTime $date
     * @return \DateTime|static
     */
    private function getNextWorkingDate(\DateTime $date){

        while(!$this->isDateWithinWorkingHours($date))
        {
            $date = TimeHelper::addOneHour($date);
        }
        $date = TimeHelper::roundToHour($date);
        return $date;
    }




}