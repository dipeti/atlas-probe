<?php
namespace Entity;
use Utils\TimeHelper;

class Project
{
    /**
     * @var \DateTime
     */
    private $startDate;
    /**
     * @var \DateInterval
     */
    private $duration;

    /**
     * Project constructor.
     * @param \DateTime $startDate
     * @param \DateInterval $duration
     */
    public function __construct(\DateTime $startDate = null, \DateInterval $duration = null)
    {
        $this->startDate = $startDate;
        $this->duration = $duration;
    }





    /**
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     */
    public function setStartDate($startDate)
    {
         $this->startDate = TimeHelper::roundToHour($startDate);
    }

    /**
     * @return \DateInterval
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param int $hours
     */
    public function setDuration($hours)
    {
        $this->duration = \DateInterval::createFromDateString($hours.' hours');
    }



}