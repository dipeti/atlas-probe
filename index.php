<?php
/**
 * @author Peter Dinya
 */
use Entity\Project;
use Entity\Solver;
const TIME = 't';
const DURATION = 'd';
require_once 'vendor\autoload.php';

$time = isset($_GET[TIME]) ?  explode('-',$_GET[TIME]) : [2017,9,7,12,15]; // defaults to 2017-09-07 12:15
$duration = isset($_GET[DURATION]) ?  $_GET[DURATION] : 1; //defaults to 1 hour;
$project = new Project();
$startDate = new DateTime();
try{
    if (5 !== count($time) || !is_numeric($duration))
        throw new InvalidArgumentException('The query string should like this: ?t=2017-09-07-12-15&d=1');

    $startDate->setTime($time[3],$time[4]);
    $startDate->setDate($time[0],$time[1],$time[2]);
    $project->setStartDate($startDate);

    $project->setDuration($duration);
    echo 'Project duration: '. $duration.' hours <br>';
    echo 'Expected time of start: '.\Utils\TimeHelper::getString($project->getStartDate()).'<br>';

    $solver  = new Solver($project);

    $result = $solver->solve();
    echo '<br>Estimated time of completion: ' . $result;
} catch (InvalidArgumentException $e){
    echo  $e->getMessage();
}
