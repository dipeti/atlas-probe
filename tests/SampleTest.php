<?php

use Entity\Project;
use Entity\Solver;

class SampleTest extends \PHPUnit\Framework\TestCase
{
    public function testTrueAssertsToTrue()
    {
        $this->assertTrue(true);
    }

    public function testSimpleCase()
    {
        $startDate = new DateTime('2017-09-07 12:00');
        $project = new Project();
        $project->setStartDate($startDate);
        $project->setDuration(1);
        $solver  = new Solver($project);

        $result = $solver->solve();

        $this->assertTrue($result === '2017-09-07 13:00:00');
    }


    public function testRoundingDownCase()
    {
        $startDate = new DateTime('2017-09-07 12:30');
        $project = new Project();
        $project->setStartDate($startDate);
        $project->setDuration(1);
        $solver  = new Solver($project);

        $result = $solver->solve();

        $this->assertTrue($result === '2017-09-07 13:00:00');
    }

    public function testRoundingUpCase()
    {
        $startDate = new DateTime('2017-09-07 12:31');
        $project = new Project();
        $project->setStartDate($startDate);
        $project->setDuration(1);
        $solver  = new Solver($project);

        $result = $solver->solve();

        $this->assertTrue($result === '2017-09-07 14:00:00');
    }

    public function testStartingOnWeekendCase()
    {
        $startDate = new DateTime('2017-09-09 12:31');
        $project = new Project();
        $project->setStartDate($startDate);
        $project->setDuration(1);
        $solver  = new Solver($project);

        $result = $solver->solve();

        $this->assertTrue($result === '2017-09-11 09:00:00');
    }
}